package io.renren;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import io.renren.config.MongoManager;
import io.renren.service.SysGeneratorService;
import io.renren.utils.GenUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import com.baomidou.mybatisplus.core.override.MybatisMapperProxy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RenrenApplicationTests {

	@Autowired
	private SysGeneratorService sysGeneratorService;
	@Test
	public void contextLoads() throws IOException {
		String[] tableNames = "sys_user".split(",");
		for (String tableName : tableNames) {
			//查询表信息
			Map<String, String> table = sysGeneratorService.queryTable(tableName);
			//查询列信息
			List<Map<String, String>> columns = sysGeneratorService.queryColumns(tableName);
			//生成代码
			TestGenUtils.generatorCode(table, columns,getTemplates());

		}
	}


	public static List<String> getTemplates() {
		List<String> templates = new ArrayList<String>();
//		templates.add("template/Debug.java.vm");
//		templates.add("template/Add.java.vm");
//		templates.add("template/Upd.java.vm");
//		templates.add("template/VO.java.vm");
//		templates.add("template/Controller.java.vm");
//		templates.add("template/ServiceImpl.java.vm");
//		templates.add("template/Entity.java.vm");
		templates.add("template/Mapper.xml.vm");
		return templates;
	}


	@Test
	public void creatCode() throws Exception {

		String[] tableNames = new String[]{"in_store_order","in_store_order_details","out_store_order","out_store_order_details"};
		byte[] data = sysGeneratorService.generatorCode(tableNames);

		String zipName = "code.zip";
		FileUtil.del(zipName);
		IOUtils.write(data, new FileOutputStream(zipName));
		ZipUtil.unzip(new File(zipName));


	}



}
